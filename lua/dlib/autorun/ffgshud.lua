
-- Copyright (C) 2018 DBot

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.


if CLIENT then
	include('ffgshud/init.lua')
	return
end

AddCSLuaFile('ffgshud/init.lua')
AddCSLuaFile('ffgshud/vars.lua')
AddCSLuaFile('ffgshud/basicpaint.lua')
AddCSLuaFile('ffgshud/targetid.lua')
AddCSLuaFile('ffgshud/anims.lua')
AddCSLuaFile('ffgshud/functions.lua')
AddCSLuaFile('ffgshud/binfo.lua')
AddCSLuaFile('ffgshud/dmgtrack.lua')
AddCSLuaFile('ffgshud/glitch.lua')
AddCSLuaFile('ffgshud/vehicle.lua')
AddCSLuaFile('ffgshud/killfeed.lua')
AddCSLuaFile('ffgshud/compass.lua')
AddCSLuaFile('ffgshud/wepselect.lua')
AddCSLuaFile('ffgshud/history.lua')
AddCSLuaFile('ffgshud/crosshairs.lua')
AddCSLuaFile('ffgshud/tfacompat.lua')
include('ffgshud/sv/dmgtrack.lua')
