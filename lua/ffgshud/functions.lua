
-- Copyright (C) 2018 DBot

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.


local FFGSHUD = FFGSHUD
local RealTimeL = RealTimeL
local IsValid = FindMetaTable('Entity').IsValid
local luatype = luatype

function FFGSHUD:GetAmmoDisplayText()
	local ammoReadyText = ''
	local ammoStoredText = ''
	local stored2AmmoText = ''
	local clip2AmmoText = ''

	if self:ShouldDisplayAmmo() or self:ShouldDisplaySecondaryAmmo() then
		if self:ShouldDisplayAmmoStored() then
			ammoStoredText = self:GetDisplayAmmo1()

			if self:GetDisplayMaxClip1() < self:GetDisplayClip1() then
				ammoReadyText = ('%i+%i'):format(self:GetDisplayMaxClip1(), self:GetDisplayClip1() - self:GetDisplayMaxClip1())
			else
				ammoReadyText = ('%i'):format(self:GetDisplayClip1())
			end

			if self:ShouldDisplaySecondaryAmmo() then
				local ready = self:SelectSecondaryAmmoReady()
				local stored = self:SelectSecondaryAmmoStored()

				if ready ~= -1 then
					clip2AmmoText = '/' .. self:GetDisplayClip2()
				end

				if stored ~= -1 then
					stored2AmmoText = '/' .. stored
				end
			end
		elseif self:ShouldDisplayAmmoReady() then
			if self:GetDisplayMaxClip1() < self:GetDisplayClip1() then
				ammoReadyText = ('%i+%i'):format(self:GetDisplayMaxClip1(), self:GetDisplayClip1() - self:GetDisplayMaxClip1())
			else
				ammoReadyText = ('%i'):format(self:GetDisplayClip1())
			end
		else
			if not self:ShouldDisplaySecondaryAmmo() then
				ammoReadyText = self:GetDisplayAmmo1()
			else
				ammoReadyText = ('%i/%i'):format(self:GetDisplayAmmo1(), self:GetDisplayClip2())
			end
		end

		if not self:ShouldDisplayAmmoStored() and self:ShouldDisplayAmmoStored2() then
			local stored = self:SelectSecondaryAmmoStored()

			if stored ~= -1 then
				ammoStoredText = '/' .. stored
			end
		end
	else
		ammoReadyText = '-'
	end

	if ammoStoredText ~= '' and luatype(ammoStoredText) == 'number' and ammoStoredText < 0 then
		ammoStoredText = self:GetDisplayAmmo2()
		stored2AmmoText = ''

		if ammoStoredText < 0 then
			ammoStoredText = ''
		end
	end

	return ammoReadyText, ammoStoredText, clip2AmmoText, stored2AmmoText
end

function FFGSHUD:SelectWeaponAlpha()
	return 1 - RealTimeL():progression(self.tryToSelectWeaponLast, self.tryToSelectWeaponLastEnd)
end

function FFGSHUD:GetAmmoDisplayText2()
	local ammoReadyText = ''
	local ammoStoredText = ''
	local stored2AmmoText = ''
	local clip2AmmoText = ''

	if self:ShouldDisplayAmmo_Select() or self:ShouldDisplaySecondaryAmmo_Select() then
		if self:ShouldDisplayAmmoStored_Select() then
			ammoStoredText = self:GetDisplayAmmo1_Select()

			if self:GetDisplayMaxClip1_Select() < self:GetDisplayClip1_Select() then
				ammoReadyText = ('%i+%i'):format(self:GetDisplayMaxClip1_Select(), self:GetDisplayClip1_Select() - self:GetDisplayMaxClip1_Select())
			else
				ammoReadyText = ('%i'):format(self:GetDisplayClip1_Select())
			end

			if self:ShouldDisplaySecondaryAmmo_Select() then
				local ready = self:SelectSecondaryAmmoReady_Select()
				local stored = self:SelectSecondaryAmmoStored_Select()

				if ready ~= -1 then
					clip2AmmoText = '/' .. self:GetDisplayClip2_Select()
				end

				if stored ~= -1 then
					stored2AmmoText = '/' .. stored
				end
			end
		elseif self:ShouldDisplayAmmoReady_Select() then
			if self:GetDisplayMaxClip1_Select() < self:GetDisplayClip1_Select() then
				ammoReadyText = ('%i+%i'):format(self:GetDisplayMaxClip1_Select(), self:GetDisplayClip1_Select() - self:GetDisplayMaxClip1_Select())
			else
				ammoReadyText = ('%i'):format(self:GetDisplayClip1_Select())
			end
		else
			if not self:ShouldDisplaySecondaryAmmo_Select() then
				ammoReadyText = self:GetDisplayAmmo1_Select()
			else
				ammoReadyText = ('%i/%i'):format(self:GetDisplayAmmo1_Select(), self:GetDisplayClip2_Select())
			end
		end

		if not self:ShouldDisplayAmmoStored_Select() and self:ShouldDisplayAmmoStored2_Select() then
			local stored = self:SelectSecondaryAmmoStored_Select()

			if stored ~= -1 then
				ammoStoredText = '/' .. stored
			end
		end
	else
		ammoReadyText = '-'
	end

	if ammoStoredText ~= '' and luatype(ammoStoredText) == 'number' and ammoStoredText < 0 then
		ammoStoredText = self:GetDisplayAmmo2_Select()
		stored2AmmoText = ''

		if ammoStoredText < 0 then
			ammoStoredText = ''
		end
	end

	return ammoReadyText, ammoStoredText, clip2AmmoText, stored2AmmoText
end

function FFGSHUD:CanDisplayWeaponSelect()
	return IsValid(self:PredictSelectWeapon()) and self:PredictSelectWeapon() ~= self:GetWeapon()
end

function FFGSHUD:CanDisplayWeaponSelect2()
	return IsValid(self:PredictSelectWeapon())
end

function FFGSHUD:CanHideAmmoCounter()
	return self:GetAmmoFillage1() >= 1 and self:GetAmmoFillage2() >= 1
end
