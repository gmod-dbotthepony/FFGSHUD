
-- Copyright (C) 2018 DBot

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.


local FFGSHUD = FFGSHUD
local HUDCommons = DLib.HUDCommons

FFGSHUD:RegisterCrosshairHandle('1', '0')

local CrosshairColor = FFGSHUD:CreateColorN('crosshair', 'Crosshair Outline Color', Color(0, 0, 0))
local CrosshairColorInner = FFGSHUD:CreateColorN('crosshair_inner', 'Crosshair Color', Color())

local IsValid = IsValid
local ScreenSize = ScreenSize
local surface = surface
local hook = hook
local math = math
local draw = draw
local DRAW_STATUS = false

local RealFrameTime = RealFrameTime
local CROSSHAIR_FADE = 1

function FFGSHUD:DrawCrosshairGeneric(x, y)
	if self:GetVarInVehicle() and not self:GetVarWeaponsInVehicle() then return end

	local size = ScreenSize(2.5):max(4)
	local CrosshairColor = CrosshairColor(63)

	draw.NoTexture()

	for gapSize = 1, 3 do
		local gap = size + ScreenSize(gapSize):max(gapSize * 1.5) * 1.5

		surface.SetDrawColor(CrosshairColor)
		HUDCommons.DrawCircle(x - (gap / 2):floor(), y - (gap / 2):floor(), gap, 20)
	end

	surface.SetDrawColor(CrosshairColorInner(255))
	HUDCommons.DrawCircle(x - (size / 2):floor(), y - (size / 2):floor(), size, 20)
end

function FFGSHUD:CrosshairShouldDraw(element)
	if element == 'CHudCrosshair' and not DRAW_STATUS and self.ENABLE_CROSSHAIRS:GetBool() then
		return false
	end
end

FFGSHUD:AddHookCustom('HUDShouldDraw', 'CrosshairShouldDraw')

