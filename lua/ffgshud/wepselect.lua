
-- Copyright (C) 2018 DBot

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.


local FFGSHUD = FFGSHUD
local NULL = NULL

FFGSHUD.DrawWepSelectionFadeOutStart = 0
FFGSHUD.DrawWepSelectionFadeOutEnd = 0
FFGSHUD.DrawWepSelectionFadeOutEnd2 = 0

local RealTimeL = RealTimeL
local ipairs = ipairs
local table = table
local HUDCommons = DLib.HUDCommons
local ScreenSize = ScreenSize
local LocalWeapon = LocalWeapon
local surface = surface
local LocalPlayer = LocalPlayer
local ScrWL, ScrHL = ScrWL, ScrHL
local language = language
local lastFrameAttack = false
local hud_fastswitch = GetConVar('hud_fastswitch')
local cam = cam
local Matrix = Matrix

FFGSHUD:InitializeWeaponSelector()

local function getPrintName(self)
	local class = self:GetClass()
	local phrase = language.GetPhrase(class)
	return phrase ~= class and phrase or self:GetPrintName()
end

local DRAWPOS, DRAWSIDE = FFGSHUD:DefinePosition('wepselect', 0.12, 0.11, false)
local SLOT_ACTIVE = FFGSHUD:CreateColorN('wepselect_a', 'Selector Active Slot', Color())
local SLOT_INACTIVE = FFGSHUD:CreateColorN('wepselect_i', 'Selector Inactive Slot', Color(137, 137, 137))
local SLOT_INACTIVE_BOX = FFGSHUD:CreateColorN('wepselect_i', 'Selector Inactive Box', Color(80, 80, 80))
local WEAPON_SELECTED = FFGSHUD:CreateColorN('wepselect_s', 'Selector Selected', Color(242, 210, 101))
local WEAPON_READY = FFGSHUD:CreateColorN('wepselect_r', 'Selector Weapon Ready', Color(237, 89, 152))
local WEAPON_FOCUSED = FFGSHUD:CreateColorN('wepselect_f', 'Selector Weapon Focused', Color())
local SLOT_BG = FFGSHUD:CreateColorN('wepselect_bg', 'Selector Background', Color(40, 40, 40))

local TILT_MATRIX = Matrix()
local TILT_MATRIX2 = Matrix()
TILT_MATRIX:SetAngles(Angle(0, -1.5, 0))
TILT_MATRIX2:SetAngles(Angle(0, 1.5, 0))
local render = render
local TEXFILTER = TEXFILTER

function FFGSHUD:DrawWeaponSelection()
	if not self.ENABLE_WEAPON_SELECT:GetBool() then return end

	if not self:ShouldDrawWeaponSelection() and not HUDCommons.IsInEditMode() then return end
	local x, y = DRAWPOS()
	local side = DRAWSIDE()
	--local x, y = ScrWL() * 0.12, ScrHL() * 0.11
	local spacing = ScreenSize(1.5)
	local alpha = HUDCommons.IsInEditMode() and 255 or (1 - RealTimeL():progression(self.DrawWepSelectionFadeOutStart, self.DrawWepSelectionFadeOutEnd)) * 255
	local inactive, bg, bgb = SLOT_INACTIVE(alpha), SLOT_BG(alpha * 0.75), SLOT_INACTIVE_BOX(alpha * 0.7)
	local activeWeapon = LocalWeapon()
	local boxSpacing = ScreenSize(8)
	local boxSpacing2 = boxSpacing * 3
	local unshift = ScreenSize(1.5)
	local sH = ScrHL()

	render.PushFilterMin(TEXFILTER.ANISOTROPIC)
	render.PushFilterMag(TEXFILTER.ANISOTROPIC)
	local TILT = false

	if side == 'LEFT' then
		cam.PushModelMatrix(TILT_MATRIX)
		TILT = true
	elseif side == 'RIGHT' then
		cam.PushModelMatrix(TILT_MATRIX2)
		x = x - ScreenSize(12)
		y = y - ScreenSize(12)
		TILT = true
	end

	local RIGHT = side == 'RIGHT'
	local iStart, iEnd, iStep = 1, 6, 1

	if RIGHT then
		iStart, iEnd, iStep = 6, 1, -1
	end

	for i = iStart, iEnd, iStep do
		if i ~= self.LastSelectSlot then
			local w, h = HUDCommons.WordBox(i, self.SelectionNumber.REGULAR, x, y, inactive, bg)

			for i = 1, (self.WeaponListInSlots[i] and #self.WeaponListInSlots[i] or 0) do
				HUDCommons.DrawBox(x - unshift, y + (i - 1) * (spacing + h * 0.35) + h, w, h * 0.35, bgb)
			end

			if RIGHT then
				x = x - w - spacing
			else
				x = x + w + spacing
			end
		else
			local w, h = 0, 0

			if not RIGHT then
				w, h = HUDCommons.WordBox(i, self.SelectionNumberActive.REGULAR, x, y, SLOT_ACTIVE(alpha), bg)
			else
				surface.SetFont(self.SelectionNumberActive.REGULAR)
				w, h = surface.GetTextSize(i)
			end

			local Y = y + h + spacing
			local maxW = ScreenSize(90)

			if #self.WeaponListInSlot ~= 0 then
				surface.SetFont(self.SelectionText.REGULAR)

				local isInActiveCategory = false
				local weaponPoint = 0

				for i, weapon in ipairs(self.WeaponListInSlot) do
					if weapon:IsValid() then
						local name = getPrintName(weapon)
						local W, H = surface.GetTextSize(name)

						if weapon == self.SelectWeapon then
							weaponPoint = i
							if weapon ~= activeWeapon then
								maxW = maxW:max(W + boxSpacing2)
							else
								maxW = maxW:max(W + ScreenSize(4) + boxSpacing2)
								isInActiveCategory = true
							end
						elseif weapon == activeWeapon then
							maxW = maxW:max(W + ScreenSize(4) + boxSpacing2)
							isInActiveCategory = true
						else
							maxW = maxW:max(W + boxSpacing2)
						end
					end
				end

				if RIGHT then
					if isInActiveCategory then
						w, h = HUDCommons.WordBox(i, self.SelectionNumberActive.REGULAR, x - maxW - ScreenSize(3.75), y, SLOT_ACTIVE(alpha), bg)
					else
						w, h = HUDCommons.WordBox(i, self.SelectionNumberActive.REGULAR, x - maxW + ScreenSize(1), y, SLOT_ACTIVE(alpha), bg)
					end

					x = x - w - maxW + ScreenSize(3)
					Y = Y + ScreenSize(2)
				end

				if isInActiveCategory and RIGHT then
					x = x - ScreenSize(0.8)
				end

				local scrollStart, scrollEnd = 1, #self.WeaponListInSlot

				if #self.WeaponListInSlot > 10 then
					scrollEnd = math.min(#self.WeaponListInSlot, math.max(weaponPoint + 5, 11))
					scrollStart = math.max(1, scrollEnd - 10)
				end

				for i = scrollStart, scrollEnd do
					local weapon = self.WeaponListInSlot[i]

					if weapon:IsValid() then
						local name = getPrintName(weapon)
						local W, H = surface.GetTextSize(name)
						local X = x - unshift

						if RIGHT then
							X = x + unshift + ScreenSize(11)

							if isInActiveCategory then
								X = X - ScreenSize(4.2)
							end
						end

						if weapon == self.SelectWeapon then
							if weapon ~= activeWeapon then
								HUDCommons.DrawBox(X, Y, maxW, H, WEAPON_READY(alpha))
								HUDCommons.SimpleText(name, nil, X + boxSpacing, Y, WEAPON_FOCUSED(alpha))
							else
								W = W + ScreenSize(4)
								HUDCommons.DrawBox(X, Y, maxW + ScreenSize(4), H, WEAPON_READY(alpha))
								local col = WEAPON_SELECTED(alpha)
								HUDCommons.DrawBox(X, Y, ScreenSize(4), H, col)
								HUDCommons.SimpleText(name, nil, X + ScreenSize(10), Y, col)
							end
						elseif weapon == activeWeapon then
							W = W + ScreenSize(4)
							HUDCommons.DrawBox(X, Y, maxW + ScreenSize(4), H, bg)
							local col = WEAPON_SELECTED(alpha)
							HUDCommons.DrawBox(X, Y, ScreenSize(4), H, col)
							HUDCommons.SimpleText(name, nil, X + ScreenSize(10), Y, col)
						else
							HUDCommons.DrawBox(X, Y, maxW, H, bg)
							HUDCommons.SimpleText(name, nil, X + boxSpacing, Y, WEAPON_READY(alpha))
						end

						Y = Y + H + spacing
					end
				end

				if not RIGHT then
					x = x + w + maxW - ScreenSize(6)
				end

				if isInActiveCategory then
					if RIGHT then
						x = x - ScreenSize(4)
					else
						x = x + ScreenSize(4)
					end
				end
			else
				if RIGHT then
					x = x - w + spacing
				else
					x = x + w + spacing
				end
			end
		end
	end

	if TILT then
		cam.PopModelMatrix()
	end

	render.PopFilterMin()
	render.PopFilterMag()
end

function FFGSHUD:CallWeaponSelectorDeny()
	self.DrawWepSelectionFadeOutStart = RealTimeL() + 0.5
	self.DrawWepSelectionFadeOutEnd = RealTimeL() + 1
end

function FFGSHUD:CallWeaponSelectorChosen(wep)
	self.DrawWepSelectionFadeOutStart = 0
	self.DrawWepSelectionFadeOutEnd = 0
end

function FFGSHUD:CallWeaponSelectorMove()
	self.DrawWepSelectionFadeOutStart = RealTimeL() + 2
	self.DrawWepSelectionFadeOutEnd = RealTimeL() + 2.5
	self.DrawWepSelectionFadeOutEnd2 = RealTimeL() + 3.5
end

function FFGSHUD:CallWeaponSelectorRefused()
	self.DrawWepSelectionFadeOutStart = RealTimeL()
end

function FFGSHUD:ThinkWeaponSelection()
	if not self.ENABLE_WEAPON_SELECT:GetBool() then return end

	local time = RealTimeL()

	if self.DrawWepSelectionFadeOutEnd < time then
		self.DrawWepSelection = false

		if self.LastSelectSlot ~= -1 and not hud_fastswitch:GetBool() then
			self.LastSelectSlot = -1
		end
	end

	if self.DrawWepSelectionFadeOutEnd2 < time then
		self:CallWeaponSelectorEndInternal()
	end
end

function FFGSHUD:LookupSelectWeapon()
	return self.SelectWeapon, self.DrawWepSelectionFadeOutStart > RealTimeL()
end

FFGSHUD:AddPostPaintHook('DrawWeaponSelection')
FFGSHUD:AddThinkHook('ThinkWeaponSelection')
