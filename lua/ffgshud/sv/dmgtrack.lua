
-- Copyright (C) 2018 DBot

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.


local net = net
local DLib = DLib

net.pool('ffgs.damagereceived')
net.pool('ffgs.damagedealt')
local IsValid = FindMetaTable('Entity').IsValid

local function damagereceived(self, dmg)
	if dmg:GetDamage() == 0 then return false end
	local players = DLib.combat.findPlayers(self)
	if not players then return false end

	local attacker = dmg:GetAttacker()
	local inflictor = dmg:GetInflictor()
	local reportedPos = dmg:GetReportedPosition()
	local validReportedPos = false
	local validAttacker = attacker:IsValid()
	local attackerClass = validAttacker and attacker:GetClass() or ''
	local cond = not dmg:IsFallDamage() and
		not attackerClass:startsWith('trigger_') and
		not attackerClass:startsWith('func_') and
		attackerClass ~= 'env_fire' and
		(not validAttacker or attacker:GetParent() ~= self) and
		attacker ~= self

	if cond then
		if reportedPos:IsZero() then
			if IsValid(inflictor) and not inflictor:IsWeapon() then
				reportedPos = inflictor:GetPos()
				validReportedPos = true
			elseif IsValid(attacker) then
				reportedPos = attacker:GetPos()
				validReportedPos = true
			end
		else
			validReportedPos = true
		end
	end

	net.Start('ffgs.damagereceived', true)
	net.WriteUInt64(dmg:GetDamageType() or 0)
	net.WriteFloat(dmg:GetDamage())

	net.WriteBool(validReportedPos)

	if validReportedPos then
		net.WriteVector(reportedPos)
	end

	net.Send(players)

	return true
end

local function damagedealt(ent, dmg)
	if dmg:GetDamage() == 0 then return end
	local self = dmg:GetAttacker()
	-- attacker is not a player
	if ent == self or not IsValid(self) or type(self) ~= 'Player' then return end
	-- entity is not alive
	-- if type(ent) ~= 'Player' and type(ent) ~= 'Vehicle' and type(ent) ~= 'NPC' and type(ent) ~= 'NextBot' then return end

	-- make sure that spectating players are also see damage sense for spectated player
	local players = DLib.combat.findPlayers(self)
	-- wtf?
	if not players then return end

	net.Start('ffgs.damagedealt', true)
	net.WriteUInt64(dmg:GetDamageType() or 0)
	net.WriteFloat(dmg:GetDamage())
	net.Send(players)
end

local function EntityTakeDamage(self, dmg)
	local status = damagereceived(self, dmg)

	if status or type(self) == 'Player' or type(self) == 'Vehicle' or type(self) == 'NPC' or type(self) == 'NextBot' then
		damagedealt(self, dmg)
	end
end

hook.Add('EntityTakeDamage', 'FFGSHUD', EntityTakeDamage, -4)
