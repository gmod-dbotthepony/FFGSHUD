
-- Copyright (C) 2018 DBot

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.


local FFGSHUD = FFGSHUD
local HUDCommons = DLib.HUDCommons

FFGSHUD.ENABLE_VEHICLE = FFGSHUD:CreateConVar('vehicle', '1', 'Enable Vehicle HUD')
local color_white = Color()

local FillageColorHealth = FFGSHUD:CreateColorN('fillage_hp', 'Fillage Color for HP', Color(80, 80, 80))
local FillageColorHealthShadow = FFGSHUD:CreateColorN('fillage_hp_sh', 'Fillage Color for HP Shadow', Color(230, 0, 0))
local VehicleName = FFGSHUD:CreateColorN('vehname', 'Vehicle Name', Color())
local HPColor = FFGSHUD:CreateColorN('vehcolor', 'Vehicle Health', Color())

local math = math
local RealTimeL = RealTimeL
local pi = math.pi * 16
local Lerp = Lerp
local lastDrawnHeight = 0
local ScreenSize = ScreenSize
local Quintic = Quintic

local function RealTimeLAnim()
	return RealTimeL() % pi
end

function FFGSHUD:DrawVehicleInfo()
	if not self.ENABLE_VEHICLE:GetBool() then return end
	if not self:GetVarAlive() then return end
	if self:GetVarVehicleName() == '' and not HUDCommons.IsInEditMode() then return end
	local time = RealTimeL()

	local x, y = self.POS_PLAYERSTATS()
	y = y - ScreenSize(10)
	local fullyVisible = self.HPBAR_VISIBLE and self.HealthFadeInEnd < time and self.HealthFadeOutStart > time

	if not fullyVisible then
		if not self.HPBAR_VISIBLE then
			y = y + lastDrawnHeight
		elseif self.HealthFadeInEnd > time then
			local fadeIn = Quintic(1 - time:progression(self.HealthFadeInStart, self.HealthFadeInEnd))
			y = y + lastDrawnHeight * fadeIn
		elseif self.HealthFadeOutStart < time then
			local fadeIn = Quintic(time:progression(self.HealthFadeOutStart, self.HealthFadeOutEnd))
			y = y + lastDrawnHeight * fadeIn
		end
	end

	lastDrawnHeight = ScreenSize(32)

	local w, h = self:DrawShadowedTextUp(self.PlayerName, self:GetVarVehicleName() ~= '' and self:GetVarVehicleName() or 'put vehicle name here', x, y, VehicleName())
	y = y - h * 0.83
	lastDrawnHeight = lastDrawnHeight + h * 0.83

	if HUDCommons.IsInEditMode() and self:GetVarVehicleName() == '' then
		self:DrawShadowedTextPercInvUp(self.VehicleHealth, 255, x, y, HPColor(), 0.4, FillageColorHealth())
		lastDrawnHeight = lastDrawnHeight + h * 0.83
	else
		local fillage = 1 - self:GetVehicleHealthFillage()

		if self:GetVarVehicleMaxHealth() > 1 then
			if fillage < 0.5 then
				w, h = self:DrawShadowedTextPercInvUp(self.VehicleHealth, self:GetVarVehicleHealth(), x, y, HPColor(), fillage, FillageColorHealth())
				lastDrawnHeight = lastDrawnHeight + h * 0.83
			else
				w, h = self:DrawShadowedTextPercCustomInvUp(self.VehicleHealth, self:GetVarVehicleHealth(), x, y, HPColor(), FillageColorHealthShadow(math.sin(RealTimeLAnim() * fillage * 30) * 64 + 130), fillage, FillageColorHealth())
				lastDrawnHeight = lastDrawnHeight + h * 0.83
			end
		elseif self:GetVarVehicleHealth() > 0 then
			w, h = self:DrawShadowedTextUp(self.VehicleHealth, self:GetVarVehicleHealth(), x, y, HPColor())
			lastDrawnHeight = lastDrawnHeight + h * 0.83
		end
	end
end

local lastDrawnHeight = 0

function FFGSHUD:DrawVehicleAmmo()
	if not self.ENABLE_VEHICLE:GetBool() then return end
	if not self:GetVarAlive() then return end
	local validAmmo = self:GetVarVehicleAmmoType() >= 0
	if not validAmmo then return end
	local time = RealTimeL()

	local x, y = self.POS_WEAPONSTATS()
	y = y + ScreenSize(3.3)

	if self.FADE_IN_AMMO ~= 0 then
		y = y - lastDrawnHeight * Quintic(self.FADE_IN_AMMO) * 0.7
	end

	lastDrawnHeight = ScreenSize(32)

	local fillage = 1 - self:GetAmmoFillageVehicle()
	local FillageColorAmmoShadow1 = Color(self.FillageColorAmmoShadow1)

	if fillage == 0 then
		FillageColorAmmoShadow1.r = 0
	elseif fillage == 1 then
		FillageColorAmmoShadow1.r = 200
	else
		FillageColorAmmoShadow1.r = math.sin(RealTimeLAnim() * fillage * 30) * 64 + 130
	end

	local w, h

	if fillage < 0.5 then
		w, h = self:DrawShadowedTextAlignedPercInv(self.AmmoAmount, validAmmo and self:GetVarVehicleAmmoClip() or 255, x, y, self.AmmoReadyColor(), fillage, self.FillageColorAmmo)
	else
		w, h = self:DrawShadowedTextAlignedPercCustomInv(self.AmmoAmount, validAmmo and self:GetVarVehicleAmmoClip() or 255, x, y, self.AmmoReadyColor(), FillageColorAmmoShadow1, fillage, self.FillageColorAmmo)
	end

	y = y - h * 0.83
	lastDrawnHeight = lastDrawnHeight + h * 0.83
end

FFGSHUD:AddPaintHook('DrawVehicleInfo')
FFGSHUD:AddPaintHook('DrawVehicleAmmo')
